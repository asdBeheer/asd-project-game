namespace Player.Model
{
    public interface IItem
    {
        public string ItemName { get; set; }
        public string Description { get; set; }

    }
}