﻿namespace Player.Model
{
    public interface IRadiationLevel
    {
        public int Level { get; set; }
    }
}
