namespace Network
{
    public enum SendAction
    {
        ReturnToSender,
        SendToClients,
        Ignore,
        Catch
    }
}