﻿namespace Network
{
    public interface IWebSocketConnection
    {
        public void Send(string message);
    }
}
