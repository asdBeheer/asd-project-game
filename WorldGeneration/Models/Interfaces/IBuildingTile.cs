﻿namespace WorldGeneration.Models.Interfaces
{
    public interface IBuildingTile : ITile
    {
        void DrawBuilding();
    }
}