namespace WorldGeneration.Models
{
    public static class TileSymbol
    {
        public const string DOOR = "/";
        public const string HOUSE = "+";
        public const string WALL = "\u25A0";
        public const string GAS = "%";
        public const string SPIKE = "^";
        public const string CHEST = "n";
        public const string DIRT = ".";
        public const string GRASS = ",";
        public const string STREET = "_";
        public const string WATER = "~";
    }
}